﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AgriZoneAPI.Models;

namespace AgriZoneAPI.Controllers
{
    //[EnableCors(origins: "http://api.agrizone.com/", headers: "*", methods: "*")]
    public class versController : ApiController
    {
        /* GET api/<controller> */
        public IEnumerable<vers> Get()
        {
            string query = "select * from tbl.Versions";
            DataTable dt = Database.get_DataTable(query);
            List<vers> vers = new List<vers>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    vers.Add(new Readvers(dr));
                }
            }
            return vers;
        }

        public string Post([FromBody]Createvers value)
        {
            string query = "insert into tbl.Versions(dtmAdd,dtmUpdate,isValid,lastVersion,currentVersion) values";
                   query += " ('" + value.dtmAdd + "','" + value.dtmUpdate + "','" + value.isValid + "','" + value.lastVersion + "','" + value.currentVersion + "')";           
            int res = Database.Execute(query);
            if (res == 1)
            {               
                return "true";
            }
            else
            {
                return "false";
            }
        }

        /* PUT api/<controller>/1 */
        public string Put(int id, [FromBody]Createvers value)
        {
            string query = "update tbl.Versions set dtmUpdate='" + value.dtmUpdate + "', lastVersion='" + value.lastVersion + "',currentVersion='" + value.currentVersion
                + "',isValid='" + value.isValid + "' where versionId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }

        /* DELETE api/<controller>/1 */
        public string Delete(int id)
        {
            string query = "delete from tbl.Versions where versionId='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }

  
   
}
