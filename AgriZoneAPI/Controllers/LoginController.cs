﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AgriZoneAPI.Models;


namespace AgriZoneAPI.Controller
{
    
    //[EnableCors(origins: "http://admin.go2donate.com/", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {        
        /* GET api/<controller> */
        public IEnumerable<Login> Get()
        {
            string query = "select * from tbl.Users";
            
            DataTable dt =Database.get_DataTable(query);
            List<Login> login = new List<Models.Login>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    login.Add(new ReadLogin(dr));
                }
            }
            return login;
        }

        /* GET api/<controller>/id */
        public IEnumerable<Login> Get(int id)
        {
            string query = "select * from tbl.Users where userid='" + id.ToString() + "'";
            DataTable dt = Database.get_DataTable(query);
            List<Login> login = new List<Models.Login>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    login.Add(new ReadLogin(dr));
                }
            }
            return login;
        }


        public IEnumerable<Login> GetLoginData(string id,string sid)
        {
            string query = "select * from tbl.Users where username='" + id.ToString() + "'";
            DataTable dt = Database.get_DataTable(query);
            List<Login> login = new List<Models.Login>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    login.Add(new ReadLogin(dr));
                }
            }
            return login;
        }

        /* POST api/<controller> */
        public string Post([FromBody]CreateLogin vals)
        {
            try
            {
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                DateTime dateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                string query = "insert into tbl.Users(dtmAdd,dtmUpdate,mobileNo,password,role,isVerified,deviceId,aadhaarNo) values" +
                    " ('" + dateTime.ToString() + "','" + dateTime.ToString() + "','" + vals.mobileNo + "','" + vals.password + "','" + vals.role
                    + "','" + vals.isVerified + "','" + vals.deviceId + "','" + vals.aadhaarNo + "')";



                string query_profile = "declare @Userid bigint select @Userid=IDENT_CURRENT('tbl.users') insert into tbl.Profie(dtmAdd,dtmUpdate,userId," +
                    "mobile) values ('" + dateTime.ToString() + "','" + dateTime.ToString() + "','1',@Userid,'" + vals.mobileNo + "')";

                string res = Database.Execute_Transaction(query, query_profile);
                if (res == "1")
                {
                    return "true";
                }
                else if (res == "3")
                {
                    return "exists";
                }
                else
                {
                    return "false";
                }
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }
        }


        /* DELETE api/<controller>/1 */
        public string Delete(int id)
        {
            string query = "delete from tbl.tblUser where userud='" + id + "'";
            int res = Database.Execute(query);
            if (res == 1)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }

    
}
