﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AgriZoneAPI.Models;

namespace AgriZoneAPI.Controllers
{
    public class ProfileController : ApiController
    {
        /* GET api/<controller> */
        public IEnumerable<Profile> Get()
        {
            string query = "select * from tbl.Profile";

            DataTable dt = Database.get_DataTable(query);
            List<Profile> profile = new List<Models.Profile>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadProfile(dr));
                }
            }
            return profile;
        }

        /* GET api/<controller>/id */
        public IEnumerable<Profile> Get(int id)
        {
            string query = "select * from tbl.Profile where ProfileId='" + id.ToString() + "'";
            DataTable dt = Database.get_DataTable(query);
            List<Profile> profile = new List<Models.Profile>(dt.Rows.Count);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    profile.Add(new ReadProfile(dr));
                }
            }
            return profile;
        }
    }
}
