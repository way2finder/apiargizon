﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AgriZoneAPI.Models
{
    public class Login
    {
        public int userId { get; set; }
        public string dtmAdd { get; set; }
        public string dtmUpdate { get; set; }
        public string isValid { get; set; }
        public string isStatus { get; set; }
        public string mobileNo { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public string isVerified { get; set; }
        public string deviceId { get; set; }
        public string aadhaarNo { get; set; }
        public string status { get; set; }
        
    }

    public class CreateLogin : Login
    {
    }

    public class ReadLogin:Login
    {
        public ReadLogin(DataRow dr)
        {
            userId = Convert.ToInt32(dr["userId"]);
            dtmAdd = dr["dtmAdd"].ToString();
            dtmUpdate = dr["dtmUpdate"].ToString();
            isValid = dr["isValid"].ToString();
            isStatus = dr["isStatus"].ToString();
            mobileNo = dr["mobileNo"].ToString();
            password = dr["password"].ToString();
            role = dr["role"].ToString();
            isVerified = dr["isVerified"].ToString();
            deviceId = dr["deviceId"].ToString();
            aadhaarNo = dr["aadhaarNo"].ToString();
            status = dr["status"].ToString();
        }
    }
}